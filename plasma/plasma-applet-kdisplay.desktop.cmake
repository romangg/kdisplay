[Desktop Entry]
Name=Fast Display Config

Comment=Quick configuration of a new display

Icon=preferences-desktop-display-randr
Type=Service
X-KDE-ServiceTypes=Plasma/Applet,Plasma/PopupApplet

X-Plasma-DefaultSize=290,150
X-Plasma-NotificationArea=true
X-Plasma-ConfigPlugins=kdisplay

X-KDE-Library=plasma_applet_kdisplay
X-KDE-PluginInfo-Author=Roman Gilg
X-KDE-PluginInfo-Email=subdiff@gmail.com
X-KDE-PluginInfo-Name=org.kde.plasma.kdisplay
X-KDE-PluginInfo-Version=@KDISPLAY_VERSION@
X-KDE-PluginInfo-Website=https://kwinft.org
X-KDE-PluginInfo-Category=System Information
X-KDE-PluginInfo-License=GPL
X-KDE-PluginInfo-EnabledByDefault=true
